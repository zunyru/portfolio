@extends('layout.master')

@section('content')
<div style="text-align: center;">
    <div class="row">


        <div class="col-md-6 col-md-offset-2">

            <div class="woe fadeIn">
                <h3 class="h3 text-center mb-3 mt-3"> {{ $page->title }}</h3>

            </div>
            <!--Featured image-->
            <div class="view overlay rounded z-depth-1-half">
                <div class="view overlay">
                    <img src="{{ Voyager::image( $page->image ) }}" style="width:100%">
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-2">
            <!--Featured image-->


            <!--Featured image-->
            {!! $page->body !!}
        </div>
    </div>



</div>
@endsection
