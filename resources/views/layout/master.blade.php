<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{setting('site.title')}}</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">



    <style>
        body {
            background-color: {{ setting('site.body_bg') }};
            color : {{ setting('site.body_text') }};
        }
        .btn.viewmore {
            background-color: {{ setting('site.viewmore_btn') }};
            color : {{ setting('site.viewmore_text') }};
        }
        .navbar.scrolling-navbar {
            background-color: {{ setting('site.navbar_bg') }};
        }

        .navbar.navbar-light .navbar-nav .nav-item .nav-link {
            color: {{ setting('site.navbar_text') }};
        }


    </style>

</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
             <a class="navbar-brand" href="{{ url('') }}">
                <img src="{{ Voyager::image(setting('site.logo')) }}"  height="30" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/portfolio') }}" >บริการ&ผลงาน <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/article') }}">บทความ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/about-us') }}">เกี่ยวกับเรา</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/contact-us') }}">ติดต่อเรา</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
</header>
<main>
    {{-- Banner --}}
    <section>
        <div>
          <img src="{{ Voyager::image(setting('site.site_banner_image')) }}" class="img-fluid"
          alt="">
      </div>
  </section>
  {{-- END Banner --}}

  {{-- Content --}}
  <div class="container">

    @yield('header')

    @yield('content')
</div>
{{-- END Content --}}

</main>

{{-- Footer --}}
<footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

    <div class="footer-copyright py-3">
        © 2019 Copyright:
        <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank"> sansu sa ma air </a>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<!-- Initializations -->
<script type=" text/javascript"> //Animations initialization new WOW().init(); </script> </body> </html>
