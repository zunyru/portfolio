<?php

namespace App\Providers;

use TCG\Voyager\Facades\Voyager;
use App\FormFields\SummernoteFormField;
use App\FormFields\SlugFormField;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
         Voyager::addFormField(SummernoteFormField::class);
         Voyager::addFormField(SlugFormField::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(255);
    }
}
